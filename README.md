# LeBonCode

Voici un exemple complet de fichier README.md pour un projet API REST Symfony 6, finalisé et prêt à être testé par d'autres utilisateurs. Ce document inclut toutes les informations nécessaires pour cloner, installer et tester l'API.

# Symfony 6 API REST avec Docker

Ce projet est une API REST construite avec Symfony 6, Docker, MySQL et Nginx. Vous pouvez utiliser Postman pour tester les endpoints de l'API.

## Prérequis

Assurez-vous d'avoir les outils suivants installés sur votre machine :
- Docker
- Docker Compose

## Installation

Suivez les étapes ci-dessous pour configurer et lancer le projet.

### 1. Cloner le dépôt

Clonez ce dépôt sur votre machine locale :

git clone https://gitlab.com/guy_roger/LeBonCode.git
cd votre-repo

### 2. installation des dependances symfony

Pour l'installation des dependances symfony, se positionné à la racine du projet et executé la requete suivante : composer install

### 3. Configuration de l'environnement Docker

docker-compose up --build pour construite les images.
docker-compose exec php bin/console doctrine:schema:update --force pour creer la bdd.
### 4. Installation de symfony 

L'application Symfony sera disponible à l'adresse http://localhost:8000.

### 5. Configuration de l'API
Utiliser Postman pour Tester l'API ou un autre outil qui permets d'envoyé des données sur API REST

## Étape 1 : Configurer Postman
Téléchargez et installez Postman si ce n'est pas déjà fait grace au lien suivant : https://www.postman.com/jp/downloads/.
Ouvrez Postman et créez une nouvelle collection pour organiser vos requêtes.
## Étape 2 : Créer une Requête POST
Cliquez sur New et sélectionnez HTTP Request.
Choisissez la méthode POST.
Entrez l'URL de l'API d'enregistrement, par exemple : http://localhost:8000/api/register.
## Étape 3 : Configurer le Corps de la Requête
Cliquez sur l'onglet Body.
Sélectionnez l'option raw.
Assurez-vous que le type est défini sur JSON.
Entrez les données JSON suivantes dans le champ de texte :
json
Copier le code
{
    "email": "test@gmail.com",
    "password": "password123",
    "phonenumber": "0788665427",
    "name": "name11",
    "firstname": "firstname11"
}
## Étape 4 : Envoyer la Requête
Cliquez sur le bouton Send pour envoyer la requête POST à l'API.
Vous devriez voir la réponse de l'API dans la section de réponse en bas.
Exemple de Réponse Attendue
La réponse de l'API peut varier en fonction de sa configuration, mais une réponse typique pourrait ressembler à ceci :

json

{
    "id": 1,
    "email": "test@gmail.com",
    "phonenumber": "0788665427",
    "name": "name11",
    "firstname": "firstname11",
}

Ensuite utilisé l'url http://127.0.0.1:8000/api/login_check ,Configurer le Corps de la Requête
Cliquez sur l'onglet Body.
Sélectionnez l'option raw.
Assurez-vous que le type est défini sur JSON.
Entrez les données JSON suivantes dans le champ de texte :
json
Copier le code
{
    "email": "test@gmail.com",
    "password": "password123",
}

Cliquez sur le bouton Send pour envoyer la requête POST à l'API.
Vous devriez voir la réponse de l'API dans la section de réponse en bas.
Exemple de Réponse Attendue
La réponse de l'API peut varier en fonction de sa configuration, mais une réponse typique pourrait ressembler à ceci :

json

{
    eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpYXQiOjE3MTg4ODAwNTgsImV4cCI6MTcxODg4MzY1OCwicm9sZXMiOlsiVVNFUiJdLCJ1c2VybmFtZSI6InRlc3RAZ21haWwuY29tIn0.PX0GqcU5od0xnqD4vfyC9JClYVSYvJopcZB7QWaQIxXtsRSgoN5kQ87599ibJhn_g77DGoXZ-M7npCG6Wff_NJTqqp_cf4HiZWMxi3Avd3R2NUW7FeKzzn7Jtf_nueWOfZNlxKoOC4Oumuj8v-EjXBKwc1J3DmbGIl_7FOxVOXUBra87lRwVywr1p6AdCExq10Q2WDShGWULQU2YDKfu600Q2AMA7FTKpa2pc04nQAqDi-mBYbccEmHn2LlVa13xIUTAF87K7CzGmG-aLegHF0WZhJ-2Jt6K11sgx8OLiG79iKA-20llO7Z7ttz1mFOwclUz3Wo8w6vgPdMhMHNoww
}

Ensuite il faut ajouter le token generé à postman et executé l'url suivante pour verifié que le token est bien fonctionnel.
http://127.0.0.1:8000/api/dashboard , si vous avez le JSon suivant :

{
    "message": "Welcome to your new controller!",
    "path": "src/Controller/DashboardController.php"
}

Cela veut dire que votre token marche et vous avez le droit d'interroger tous les urls de l'api. 

NB : le token a une durée de vie definie dans l'application et qui peu etre modifier à la demande de l'utilisateur

Voici l'ensemble des Urls present dans l'application et avec les méthodes d'envoi et recuperation de données :


- advert                     GET      ANY      ANY    /api/advert :Un utilisateur pourra récupérer la liste des annonces (/advert).
- createAdvert               POST     ANY      ANY    /api/advert :Un utilisateur pourra ajouter un produit à vendre (/advert)
- detailAdvert               GET      ANY      ANY    /api/advert/{id} :Un utilisateur pourra récupérer les informations d'une annonces (/advert/{id}) avec son id associé.
- deleteAdvert               DELETE   ANY      ANY    /api/advert/{id} :Un utilisateur pourra supprimer une annonce (/advert/{id}). la suppression rend l'annonce inactive
- patchAdvert                PATCH    ANY      ANY    /api/advert/{id} :Un utilisateur pourra modifier les informations d'une annonce (/advert/{id}). Le titre de l'annonce ne peut pas être modifié.
- search_advert              GET      ANY      ANY    /api/advert/search :Un utilisateur pourra chercher une annonce (/advert/search).
- api_app_dashboard          ANY      ANY      ANY    /api/dashboard : un utilisateur pourra verifer si le token generé fonctionne bien
- api_register               POST     ANY      ANY    /api/register  : on pourra enregistrer un nouvelle utilisateur
- api_login_check            ANY      ANY      ANY    /api/login_check : un utilisateur pourra se connecter pour récupérer le token


