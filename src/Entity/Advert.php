<?php

namespace App\Entity;


use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass:"App\Repository\AdvertRepository")]
class Advert
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;
    
    #[ORM\Column(length: 100)]
    #[Groups(["getAdvert"])]
    private ?string $title = null;

    #[ORM\Column(type: 'text')]
    #[Groups(["getAdvert"])]
    private ?string $description = null;

    #[ORM\Column(length: 100)]
    #[Groups(["getAdvert"])]
    private ?float $price = null;

    #[ORM\Column(length: 5)]
    #[Assert\Length(
        min: 5,
        max: 5,
        minMessage: 'Code postal invalide',
        maxMessage: 'Code postal invalide',
    )]
    #[Assert\Regex(
        pattern: '/[0-9]{2}[0-9]{3}/',
        message: 'Code postal invalide'
    )]
    #[Groups(["getAdvert"])]
    private ?int $zipcode = null;

    #[ORM\Column(length: 20)]
    #[Groups(["getAdvert"])]
    private ?string $city= null;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'Advert')]
    #[Groups(["getAdvert"])]
    private $user = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): static
    {
        $this->description = $description;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): static
    {
        $this->user = $user;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): static
    {
        $this->price = $price;

        return $this;
    }

    public function getZipcode(): ?string
    {
        return $this->zipcode;
    }

    public function setZipcode(string $zipcode): static
    {
        $this->zipcode = $zipcode;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): static
    {
        $this->city = $city;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): static
    {
        $this->title = $title;

        return $this;
    }


}    