<?php

namespace App\Repository;

use App\Entity\Advert;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class AdvertRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Advert::class);
    }

    public function findAdvert()
    {
        return $this->createQueryBuilder('p','u','c')
        ->join('p.advert','c')
        ->join('p.user','u')
        ->getQuery() // Récupération de la query
        ->getResult(); // Récupération du résultat de la query

    }

    /**
     * @return Advert[]
     */
    public function findByCriteria(?string $title, ?float $minPrice, ?float $maxPrice): array
    {
        $qb = $this->createQueryBuilder('a');

        if ($title !== null) {
            $qb->andWhere('a.title LIKE :title')
               ->setParameter('title', '%' . $title . '%');
        }

        if ($minPrice !== null) {
            $qb->andWhere('a.price >= :minPrice')
               ->setParameter('minPrice', $minPrice);
        }

        if ($maxPrice !== null) {
            $qb->andWhere('a.price <= :maxPrice')
               ->setParameter('maxPrice', $maxPrice);
        }

        return $qb->getQuery()->getResult();
    }
}
