<?php

namespace App\Controller;

use App\Entity\Advert;
use App\Repository\AdvertRepository;
use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;


class AdvertController extends AbstractController
{
    #[Route('/api/advert', name: 'advert', methods: ['GET'])]
    public function getAlladvert(AdvertRepository $AdvertRepository, SerializerInterface $serializer): JsonResponse
    {
        $advertList = $AdvertRepository->findAll();

        $jsonAdvertList = $serializer->serialize($advertList, 'json', ['groups' => 'getAdvert']);
        return new JsonResponse($jsonAdvertList, Response::HTTP_OK, [], true);
    }

    #[Route('/api/advert', name:"createAdvert", methods: ['POST'])]
    public function createAdvert(Request $request, SerializerInterface $serializer, EntityManagerInterface $em, UrlGeneratorInterface $urlGenerator, UserRepository $userRepository): JsonResponse 
    {
        $advert = $serializer->deserialize($request->getContent(), Advert::class, 'json');

        // Récupération de l'ensemble des données envoyées sous forme de tableau
        $content = $request->toArray();

        // Récupération de l'iduser. S'il n'est pas défini, alors on met -1 par défaut.
        $idUser = $content['iduser'] ?? -1;

        // On cherche l'utlisateur qui correspond et on l'assigne au produit.
        // Si "find" ne trouve pas l'utlisateur, alors null sera retourné.
        $advert->setUser($userRepository->find($idUser));

        $em->persist($advert);
        $em->flush();

        $jsonAdvert = $serializer->serialize($advert, 'json', ['groups' => 'getAdvert']);

        $location = $urlGenerator->generate('detailAdvert', ['id' => $advert->getId()], UrlGeneratorInterface::ABSOLUTE_URL);

        return new JsonResponse($jsonAdvert, Response::HTTP_CREATED, ["Location" => $location], true);
    }

    #[Route('/api/advert/{id}', name: 'detailAdvert', methods: ['GET'])]
    public function getDetailAdvert(Advert $advert, SerializerInterface $serializer): JsonResponse 
    {
        $jsonAdvert = $serializer->serialize($advert, 'json', ['groups' => 'getAdvert']);
        return new JsonResponse($jsonAdvert, Response::HTTP_OK, [], true);
    }
    
    #[Route('/api/advert/{id}', name: 'deleteAdvert', methods: ['DELETE'])]
    public function deleteAdvert(Advert $advert, EntityManagerInterface $em): JsonResponse 
    {
        $em->remove($advert);
        $em->flush();
        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }

    #[Route('/api/advert/{id}', name:"patchAdvert", methods:['PATCH'])]
    public function patchAdvert(Request $request, SerializerInterface $serializer, Advert $currentAdvert, EntityManagerInterface $em, UserRepository $userRepository): JsonResponse 
    {
        //recuperation du title avant modification par le Patch
        $firstTitle=$currentAdvert->getTitle();

        $updatedAdvert = $serializer->deserialize($request->getContent(), 
                Advert::class, 
                'json', 
                [AbstractNormalizer::OBJECT_TO_POPULATE => $currentAdvert]);
        $content = $request->toArray();
        $idUser = $content['idUser'] ?? -1;
        $updatedAdvert->setUser($userRepository->find($idUser));

        //mise à jour du title après envoi du Json
        $updatedAdvert->setTitle($firstTitle);

        $em->persist($updatedAdvert);
        $em->flush();
        return new JsonResponse(null, JsonResponse::HTTP_NO_CONTENT);
   }

   #[Route('/api/advert/search', name: 'search_advert', methods:['GET'])]
    public function searchAdvert(Request $request, AdvertRepository $advertRepository): JsonResponse
    {
        // Récupération des paramètres de la requête
        $title = $request->query->get('title');
        $minPrice = $request->query->get('minPrice');
        $maxPrice = $request->query->get('maxPrice');

        // Conversion des prix en float si nécessaire
        $minPrice = $minPrice !== null ? (float)$minPrice : null;
        $maxPrice = $maxPrice !== null ? (float)$maxPrice : null;

        // Recherche des annonces selon les critères
        $advert = $advertRepository->findByCriteria($title, $minPrice, $maxPrice);

        // Vérification s'il y a des résultats
        if (empty($advert)) {
            throw new NotFoundHttpException('No adverts found for the given criteria');
        }

        // Retourner les résultats
        return $this->json($advert, Response::HTTP_OK, [], ['groups' => 'advert:read']);
    }


}
